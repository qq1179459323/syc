import React, {Component} from 'react';
import bg from './img/fourIndex.png';
import BallCharts from '../ball/Ball3d';
import Category from '../ball/Category';

class QuadrantDiagram extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div style={{width: '1125px', height: '800px', position: 'absolute', left: '180px', top: '114px'}}>
        <div style={{width: '306px', height: '267px', position: 'absolute', left: '0px', top: '123px'}}>
          <img src={bg} style={{width: '100%', height: '100%'}}/>
        </div>

        <div id={'box'} style={{width: '1125px', height: '800px'}}></div>

      </div>
    )
  }

  componentDidMount() {
    let box = document.getElementById('box');

    let ballObj = BallCharts(box, function (info) {

    });
    ballObj.init();

    ballObj.show([
      {
        name: "苹果",
        districtName: "111",
        id: "AF01001",
        quadrant: 'a',
        x: 200,
        y: 200
      },
      {
        name: "鸡蛋",
        districtName: "222",
        id: "AL05001",
        x: -200,
        y: 200
      },
      {
        name: "猪肉",
        districtName: "333",
        id: "AL01002",
        x: 200,
        y: -200
      },
      {
        name: "猪肉",
        districtName: "444",
        id: "AL01002",
        x: -200,
        y: -200
      },
       {
        name: "猪肉",
        districtName: "555",
        id: "AL01004",
        x: 0,
        y: 0
      },
       {
        name: "猪肉",
        districtName: "666",
        id: "AL01005",
        x: -100,
        y: -100
      },

      {
        name: "活草鱼",
        districtName: "777",
        id: "AM01002",
        x: 100,
        y: 100
      }]);
  }
}

export default QuadrantDiagram;